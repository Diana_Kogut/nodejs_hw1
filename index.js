const http = require('http');
const fs = require('fs');
const url = require('url');

const PORT = 8080,
    OKAY = 200,
    CLIENT_ERROR = 400,
    SERVER_ERROR = 500;

let logs = [];


try {
    logs = JSON.parse(fs.readFileSync('./logs.json'));
} catch (err) {
    if (err.code === 'ENOENT') {
        fs.writeFileSync('./logs.json', '{ "logs" : [] }');
        logs = JSON.parse(fs.readFileSync('./logs.json'));
    }
}

const createNewLog = (message) => {
    const newLog = {
        message: message,
        time: (new Date()).getTime()
    };

    logs.logs.push(newLog);
    fs.writeFile('./logs.json', JSON.stringify(logs), (err) => {
        if (err) {
            response.writeHead(SERVER_ERROR, { 'Content-type': 'text/html' });
            response.end(`Something went wrong`);
        }
    });
};

const logsFromRange = (from, to) => logs.logs.filter(item => item.time >= from && item.time <= to);

const validateParams = (filename, content) => {
    const template = /.txt|.js|.html|.php|.doc|.gitignore|.css|.json|.pdf/;
    return template.test(filename) && content.length > 1;
};

module.exports = () => {

    http.createServer((request, response) => {

        const { pathname, query } = url.parse(request.url, true);

        if (request.method === 'GET') {

            logs.logs = query.from && query.to ? logsFromRange(query.from, query.to) : logs.logs;
            console.log(logsFromRange(query.from, query.to))

            fs.readFile(`./${pathname === '/logs' ? './logs.json' : pathname}`, (err, content) => {
                if (err) {
                    createNewLog(`Could not read file named '${pathname}'`);
                    response.writeHead(CLIENT_ERROR, { 'Content-type': 'text/html' });
                    response.end(`Oops! There is no file named '${pathname}'`);
                } else {
                    if (pathname !== '/logs') {
                        createNewLog(`The file with name '${pathname}' viewed`);
                        response.end(content);
                    }
                    response.writeHead(OKAY, { 'Content-type': 'text/html' });
                    response.end(JSON.stringify(logs));
                }
            });
        } else {
            fs.open('./file', (err) => {
                if (err && err.code === 'ENOENT') {
                    fs.mkdirSync('./file');
                } else if (!validateParams(query.filename, query.content)) {
                    response.writeHead(CLIENT_ERROR, { 'Content-type': 'text/html' });
                    console.log('STATUS', request.statusCode)
                    response.end('Please enter correct information');
                }
                fs.writeFile(`./file/${query.filename}`, query.content, (error) => {
                    if (error) {
                        createNewLog(`Failed to create file named '${query.filename}'`);
                        response.writeHead(SERVER_ERROR, { 'Content-type': 'text/html' });
                        response.end(`Sorry, something went wrong`);
                    }
                });
                createNewLog(`The file with name '${query.filename}' saved`);
                response.end('File successfully created');
            });
        }

    }).listen(process.env.PORT || PORT, () => console.log('Server started'));
};